#!/bin/bash
#
# Sets up a virtual environment (venv) for the temperature sensor, then installs its prerequisites.
# This script is meant for local development.

set -eux

SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
IOT_CLIENT_DIR="${SELF_DIR}/../sensor"

pushd "${IOT_CLIENT_DIR}" >/dev/null

if [[ ! -f root.ca.pem ]]; then
  curl -fsSL https://www.amazontrust.com/repository/AmazonRootCA1.pem -o root.ca.pem
  chmod 644 root.ca.pem
fi

if [ -z "`which virtualenv`" ]; then
  echo "virtualenv is not installed. Please install it and try again." 1>&2
  exit 1
fi

if [ ! -d venv ]; then
  echo "Initializing virtualenv environment ..."
  PYTHON3_PATH=`which python3`
  virtualenv --python="${PYTHON3_PATH}" venv
fi

OLD_VIRTUAL_ENV="${VIRTUAL_ENV:-}"

if [ -z "${OLD_VIRTUAL_ENV}" -o "${OLD_VIRTUAL_ENV}" != "${PWD}/venv" ]; then
  . venv/bin/activate
fi

pip install -r requirements.txt

if [ -z "${OLD_VIRTUAL_ENV}" ]; then
  echo "Virtualenv is not active; please activate it by running \"source venv/bin/activate\"." 1>&2
  exit 10
fi

if [ "${OLD_VIRTUAL_ENV}" != "${PWD}/venv" ]; then
  echo "The active virtualenv environment (${OLD_VIRTUAL_ENV}) does not match the current project; please run \"source venv/bin/activate\"." 1>&2
  exit 11
fi

popd >/dev/null
