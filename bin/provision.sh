#!/bin/bash

set -euo pipefail

SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
KEYS_DIR="${KEYS_DIR:-${SELF_DIR}/../../cloud/keys}"
PROVISIONING_DIR="${SELF_DIR}/../provisioning"
SENSOR_DIR="${SELF_DIR}/../sensor"

pushd "${PROVISIONING_DIR}" >/dev/null

# Ansible 2.17 is the last version that supports Python 3.7, 
# which is the latest one that is easily installable on my Raspberry Pi.
ANSIBLE_VERSION="2.17.0"

# Provision the device.
docker run -it --rm \
  -v "${HOME}/.ssh:/root/.ssh:ro" \
  -v "${KEYS_DIR}:/keys:ro" \
  -v "${PROVISIONING_DIR}:/provisioning" \
  -v "${SENSOR_DIR}:/sensor:ro" \
  -w /provisioning \
  "alpine/ansible:${ANSIBLE_VERSION}" \
  ansible-playbook \
    --vault-password-file=.ansible-vault-pw \
    --inventory=inventory.yaml \
    --extra-vars=@/keys/variables.yaml \
    playbook.yaml

popd >/dev/null
