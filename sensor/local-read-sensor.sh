#!/bin/bash

set -eux

OLD_VIRTUAL_ENV="${VIRTUAL_ENV:-}"

if [ -z "${OLD_VIRTUAL_ENV}" -o "${OLD_VIRTUAL_ENV}" != "${PWD}/venv" ]; then
  . venv/bin/activate
fi

python read-sensor.py --config-file local-config.json --log-level Info
